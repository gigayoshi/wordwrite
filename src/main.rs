use std::fs::File;
use std::io::{Read, Write};
use std::process::exit;

use zip::{ZipArchive, ZipWriter};

fn main() {
    real_main();
}

fn real_main() {
    let args: Vec<_> = std::env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} <filename>", args[0]);
        exit(0);
    }
    let fname = std::path::Path::new(&*args[1]);

    let original_zip_file = match File::open(&fname) {
        Ok(file) => file,
        Err(_) => {
            println!("is the file path correct?");
            exit(0);
        }
    };

    let mut archive = match ZipArchive::new(&original_zip_file) {
        Ok(file) => file,
        Err(_) => {
            println!("failed opening the file");
            exit(0);
        }
    };
    let new_settings = match new_settings_contents(&mut archive) {
        Ok(new_settings) => new_settings,
        Err(err) => {
            println!("{}", err);
            exit(0);
        }
    };
    let new_file_name = match fname.extension() {
        Some(ext) => format!(
            "{} - password removed.{}",
            fname.file_stem().unwrap().to_str().unwrap(),
            ext.to_str().unwrap()
        ),
        None => format!(
            "{} - password removed",
            fname.file_stem().unwrap().to_str().unwrap()
        ),
    };
    let new_file = std::fs::File::create(&new_file_name).unwrap();

    // let mut new_zipfile = ZipWriter::new_append(original_zip_file).unwrap();
    let mut new_zipfile = ZipWriter::new(new_file);
    for i in 0..archive.len() {
        let file = archive.by_index(i).unwrap();
        let outpath = match file.enclosed_name() {
            Some(path) => path.to_owned(),
            None => continue,
        };
        if outpath.to_str() != Some("word/settings.xml") {
            new_zipfile.raw_copy_file(file).unwrap();
        }
    }

    new_zipfile
        .start_file("word/settings.xml", Default::default())
        .unwrap();
    new_zipfile.write_all(new_settings.as_bytes()).unwrap();
    new_zipfile.finish().unwrap();
    println!(
        "succesfully removed the password! New file is located at: \"{}\"",
        new_file_name
    );
}

fn new_settings_contents(archive: &mut zip::ZipArchive<&File>) -> Result<String, &'static str> {
    if let Ok(mut file) = archive.by_name("word/settings.xml") {
        let mut settings = String::new();
        match file.read_to_string(&mut settings) {
            Ok(_) => (),
            Err(_) => return Err("error opening the settings file"),
        };
        let (beggining, end) = match find_password(&settings) {
            Ok((beggining, end)) => (beggining, end),
            Err(err) => return Err(err),
        };

        let document_protection = &settings[beggining..end];
        let new_settings = settings.replace(document_protection, "");
        Ok(new_settings)
    } else {
        Err("could not find the settings file")
    }
}

fn find_password(file: &str) -> Result<(usize, usize), &'static str> {
    if let Some(beggining) = file.find("<w:documentProtection") {
        if let Some(end) = file[beggining..file.len()].find("/>") {
            Ok((beggining, end + beggining + 2))
        } else {
            Err("file possibly not encrypted?")
        }
    } else {
        Err("file possibly not encrypted?")
    }
}
